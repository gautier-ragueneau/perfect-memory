
The goal of this POC/excercice is to collect data from a server A and send it to server B that it will be store and displays collected metrics from server A. 

Lot of stack of monitoring already exist for this use case like Telegraf InfluxDB Grafana, ELK, Prometheus, so I'll try to imitate and lighter these solutions

- [Architecture](#architecture)
  - [Why these tools](#why-these-tools)
- [Steps](#steps)
  - [Collect data](#collect-data)
  - [Docker](#docker)
  - [Send data to InfluxDB](#send-data-to-influxdb)
  - [Grafana](#grafana)
- [Usage](#usage)
  - [collect_data.sh](#collect_datash)
  - [writer.jar](#writerjar)
  - [grafana](#grafana-1)
- [Improvements](#improvements)


# Architecture

To achieve this excercice I need 
- 2 VM to build my project, a VM `A` for collecting metrics and a VM `B` to stock and diplay metrics -> EC2 by AWS 
- A database to store metrics -> InfluxDB
- A dashboarding solution -> Grafana (imposed)
- JAVA / Docker / My brain

## Why these tools

InfluxDB : 
> I personnaly use this database for my home automation (Home assistant), linked with Chronograf I can run queries and display data from my database. There are API/Libraries for JAVA. InfluxDB is fully compatible as datasource for Grafana.

AWS EC2 : 
> I had already used AWS for some POC and testing, it's easy and fast to create an instance with EC2. AWS includes 750 free hours, so that was enough for this exercise. I chose centos 8 as the operating system for my VMs.


# Steps
## Collect data
The first step was to collect data (load average, rss and cpu time) from server A. 

Script `collect_data.sh` run `ps` command with specific arguments to get statistics about host, in our case I need to collect the RSS (per process) and the CPUTIME (cummlative CPU time used per process).

But the purpose of this script is to get these statistics in JSON format, to achieve it I used `jq` command and a jq template `main_template.jq`. 

To get laod average I simply parsed content of `/proc/loadavg`

One more thing, the output folder of these JSON files must be parametrable. 

3 possibilties :
- Use output folder as argument of this script
- Use a config file that contains all parametrables variables
- Use global variable inside my script

I decided to go with the third option, because I think we don't need to change this output file/folder often and it's easier to understand the script as a whole.

We need to collect the metrics every minute, the easiest way to do this is to add a cron that runs the script every minute. I could have coded a loop in my script to avoid having a cron job, but if the system reboots, the script will not run anymore and I prefer to split responsibilities : script for collecting and write data and cron for deamon

## Docker 
As requested, Grafana, InfluxDB and deamon should be dockered.

To replay and store this configuration, I decided to use docker-compose (`docker-compose.yml` in git). 

The file in [Docker/](https://gitlab.com/gautier-ragueneau/perfect-memory/-/tree/main/Docker/) deploys InfluxDB, Grafana and expose their own port. It also creates the database and users / password for InfluxDB.

The `docker-compose.yml` in [JAVA Project](https://gitlab.com/gautier-ragueneau/perfect-memory/-/tree/main/Java/perfect_memory_client) deploys the deamon to write metrics in InfluxDB

During my tests I also deployed Telegraf and Chronograf with a docker-compose file
>Telegraf helped me compare the metrics between my script and Telegraf's.

>Chronograf helped me to visualize the data sent by my script.

## Send data to InfluxDB
To send metrics to InfluxDB, I preferred to use their JAVA library.

I wrote a simple java application that does :
- Get as first argument a folder (that contains JSON files) or a JSON file (See [Usage](#usage))
- Deserialize json file(s) to a JAVA Object -> I used `gson` library
- Connects & write metrics to the InfluxDB database


## Grafana
Due to the simple structure of our metrics, it was easy to set up dashboard.
I added variables fields at the top of the dashboard to filter by host or process.

To make things more interesting, I wrote a fake stress script to generate spikes for load average (See [fake_stress.sh](https://gitlab.com/gautier-ragueneau/perfect-memory/-/blob/main/Scripts/fake_stress.sh))

# Usage

## collect_data.sh
This script has no argument

If you want to update the folder name, just edit the global variables at the beginning of the script. 
```bash
FOLDER_PATH=METRICS  # Absolute path of metrics
FILE_NAME=metric_${CURRENT_TIMESTAMP}.json # File timestamped name 
# ---
CURRENT_DATETIME=$(date +"%d-%m-%YT%T%:z") # Used to timestamped metrics
CURRENT_TIMESTAMP=$(date +"%s")  # Used to timestamped JSON files
```

## writer.jar

The [source](https://gitlab.com/gautier-ragueneau/perfect-memory/-/tree/main/Java/perfect_memory_client)  are available on gitlab. 

writer.jar can have 3 differents arguments : 
- auto <root_folder>
- </path/to/root_folder>
- </path/to/filename/metric.json>

With the 'auto' mode, the script runs a deamon that watch a specific folder describe as second argument of the script `<root folder>` and writes it to InfluxDB.

If you pass a path with a file, the script will insert in InfluxDB only the metrics defined as argument.

If you pass a folder as the first argument, the script will insert ALL metrics in that folder.

## grafana
You can acces to the dashboard : [Perfect Memory Dashboard](http://ec2-54-175-203-106.compute-1.amazonaws.com:3000/d/hMCxzEo7k/perfect-memory-dashboard?orgId=1&refresh=1m&from=now-1h&to=now&var-Host=All&var-Process=All) (The VM can be turned off, ask me to start it)

You can change variable to filter by process or by node

# Improvements 

- Use certificates to expose services (Grafana, InfluxDB, ...)
- Use a Infrastructure as Code to deploy tools on servers
- Check code coverage with Sonarqube
- Add unit tests to improve coverage
- Push artifact with gitlab CI to an artifact repository manager. 
- Use field "time" from the JSON to send correct timestamped data to InfluxDB