import Utils.InfluxHelper;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.WatchService;


public class WriterDaemonTest {

    private WriterDaemon writerDaemon;

    private WatchService watchService;
    private InfluxHelper influxHelper;

    @Before
    public void before() throws IOException {
        watchService = FileSystems.getDefault().newWatchService();
        influxHelper = InfluxHelper.getInstance();
    }


    @Test(expected = IOException.class)
    public void testFile() throws IOException, InterruptedException {
        ClassLoader classLoader = this.getClass().getClassLoader();
        File fakeFile = new File(classLoader.getResource("METRICS/metric.json").getFile());
        writerDaemon = new WriterDaemon(fakeFile);
        writerDaemon.start(influxHelper, watchService);
    }
}
