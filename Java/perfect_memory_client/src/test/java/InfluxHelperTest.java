import Model.Data;
import Model.Process;
import Utils.InfluxHelper;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class InfluxHelperTest {

    private InfluxHelper influxHelper;

    @Before
    public void before(){
        influxHelper = InfluxHelper.getInstance();
    }


    @Test
    public void parseJsonTest() throws FileNotFoundException {
        ClassLoader classLoader = this.getClass().getClassLoader();
        File resourceFile = new File(classLoader.getResource("METRICS/metric.json").getFile());
        Data data = influxHelper.parseJson(resourceFile.getAbsolutePath());
        assertNotNull(data);
        assertNotNull(data.getMetrics());
        assertEquals(getExpectProcess(), data.getMetrics().getProcess().get(1));
    }

    @Test(expected = FileNotFoundException.class)
    public void parseJsonWithWrongPathTest() throws FileNotFoundException {
        influxHelper.parseJson("wrong/path/metric.json");
    }


    private Process getExpectProcess() {
        return new Process("debian-gnu-linux-10", 10139, "java", 108740, 37);
    }

}
