import Utils.FileUtil;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;



public class FileUtilTest {

    FileUtil fileUtil;

    @Before
    public void before(){
        fileUtil = new FileUtil();
    }

    @Test
    public void checkBasicFileExtension() {
        assertEquals(fileUtil.getFileExtension("filename.json"), "json");
    }

    @Test
    public void checkComposeFileExtension() {
        assertNotEquals(fileUtil.getFileExtension("filename.tar.gz"), "tar.gz");
        assertEquals(fileUtil.getFileExtension("filename.tar.gz"), "gz");
    }

}
