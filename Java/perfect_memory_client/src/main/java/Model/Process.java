package Model;

import com.influxdb.annotations.Column;
import com.influxdb.annotations.Measurement;

@Measurement(name = "Process")
public class Process {

    public Process(String host, int pid, String name, long rss, long cputime) {
        this.host = host;
        this.pid = pid;
        this.name = name;
        this.rss = rss;
        this.cputime = cputime;
    }

    @Column(tag = true)
    private String host;

    @Column
    private int pid;

    @Column(tag = true)
    private String name;

    @Column
    private long rss;

    @Column
    private long cputime;

    @Override
    public String toString() {
        return "Process{" +
                "host='" + host + '\'' +
                ", pid=" + pid +
                ", name='" + name + '\'' +
                ", rss=" + rss +
                ", cputime=" + cputime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Process process = (Process) o;

        if (host != null ? !host.equals(process.host) : process.host != null) return false;
        return name != null ? name.equals(process.name) : process.name == null;
    }

    @Override
    public int hashCode() {
        int result = host != null ? host.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
