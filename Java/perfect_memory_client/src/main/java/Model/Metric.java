package Model;

import com.influxdb.annotations.Column;
import com.influxdb.annotations.Measurement;

import java.util.List;

@Measurement(name = "Metric")
public class Metric {

    public Metric(Node node, List<Process> process) {
        this.node = node;
        this.process = process;
    }

    @Column(tag = true)
    private Node node;

    @Column
    private List<Process> process;

    public Node getNode() {
        return node;
    }

    public List<Process> getProcess() {
        return process;
    }

    @Override
    public String toString() {
        return "Metric{" +
                "node=" + node +
                ", process=" + process.toString() +
                '}';
    }
}
