package Model;

import com.influxdb.annotations.Column;
import com.influxdb.annotations.Measurement;

@Measurement(name = "Node")
public class Node {

    public Node(double value, String host) {
        this.value = value;
        this.host = host;
    }

    @Column
    private double value;

    @Column(tag = true)
    private String host;

    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                ", host='" + host + '\'' +
                '}';
    }
}
