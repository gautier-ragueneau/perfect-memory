package Model;

import com.influxdb.annotations.Column;
import com.influxdb.annotations.Measurement;

import java.util.prefs.PreferenceChangeEvent;

@Measurement(name = "Data")
public class Data {

    public Data(Metadata metadata, Metric metrics) {
        this.metadata = metadata;
        this.metrics = metrics;
    }

    @Column
    Metadata metadata;

    @Column
    Metric metrics;

    public Metadata getMetadata() {
        return metadata;
    }

    public Metric getMetrics() {
        return metrics;
    }

    @Override
    public String toString() {
        return "Data{" +
                "metadata=" + metadata.toString() +
                ", metrics=" + metrics.toString() +
                '}';
    }
}
