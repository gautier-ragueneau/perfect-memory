package Model;

import com.influxdb.annotations.Column;
import com.influxdb.annotations.Measurement;

@Measurement(name = "Metadata")
public class Metadata {

    public Metadata(String node, String time) {
        this.node = node;
        this.time = time;
    }

    @Column(tag = true)
    private String node;

    @Column
    private String time; //TODO Use java format Date -> convert string to Date ?

    @Override
    public String toString() {
        return "Metadata{" +
                "node='" + node + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
