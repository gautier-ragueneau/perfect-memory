import Utils.FileUtil;
import Utils.InfluxHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;


public class Main {

    private static InfluxHelper influxHelper;

    public static void main (String[] args) throws Exception {
        if(args.length == 0){
            System.out.println("Usage : " +
                    "\n\t- java -jar writer.jar <filename.json>" +
                    "\n\t- java -jar writer.jar /path/to/all/metrics/" +
                    "\n\t- java -jar writer.jar auto /path/to/all/metrics/");
            System.exit(1);
        }
        influxHelper = InfluxHelper.getInstance();
        if(args[0].equals("auto") && args.length == 2){
            //Get last file, run auto by a daemon
            File directory = new File(args[1]);
            if(!directory.isDirectory()){
                System.out.println("For auto mode, you must specify the root directory of your metrics");
                System.exit(1);
            }
            autoMode(directory);
        } else {
            // For test or debug, use file or folder as first arg
            debugMode(new File(args[0]));
        }
        influxHelper.closeClient();
        System.exit(0);
    }

    private static void debugMode(File file) throws FileNotFoundException {
        if(file == null){
            System.out.println("No metrics found");
            System.exit(1);
        }
        if (file.isDirectory() && file.listFiles() != null) {
            //Scan directory and write all json
            influxHelper.writeDataFromDirectory(file.listFiles());
        } else if (file.isFile() && new FileUtil().getFileExtension(file.getName()).equals("json")) {
            influxHelper.writeData(file.getAbsolutePath());
        }
    }

    private static void autoMode(File directory) {
        try {
            WriterDaemon daemon = new WriterDaemon(directory);
            daemon.start(influxHelper, FileSystems.getDefault().newWatchService());
        } catch (IOException | InterruptedException e) {
            InfluxHelper.getInstance().closeClient();
            e.printStackTrace();
        }
    }


}
