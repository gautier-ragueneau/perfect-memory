import Utils.FileUtil;
import Utils.InfluxHelper;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

public class WriterDaemon {

    private final File metricsFolder;

    public WriterDaemon(File metricsFolder){
        this.metricsFolder = metricsFolder;
    }

    public void start(InfluxHelper influxHelper, WatchService watchService) throws IOException, InterruptedException {
        Path path = metricsFolder.toPath();
        path.register(watchService, ENTRY_CREATE);
        boolean poll = true;
        while (poll) {
            WatchKey key = watchService.take();
            for (WatchEvent<?> event : key.pollEvents()) {
                File lastMetric = new File(metricsFolder, ((Path) event.context()).toString());
                if (lastMetric.isFile() && new FileUtil().getFileExtension(lastMetric.getName()).equals("json")) {
                    influxHelper.writeData(lastMetric.getAbsolutePath());
                }
            }
            poll = key.reset();
        }
    }
}
