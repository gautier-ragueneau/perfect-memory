package Utils;

import Model.Data;
import Model.Process;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.WriteApiBlocking;
import com.influxdb.client.domain.WritePrecision;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class InfluxHelper {


    private static Gson gson;
    private static InfluxDBClient client;

    private InfluxHelper() {
    }

    private static InfluxHelper INSTANCE = new InfluxHelper();

    public static InfluxHelper getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new InfluxHelper();
            initClient();
        }
        return INSTANCE;
    }

    public void writeData(String path) throws FileNotFoundException {
        if (client == null) {
            initClient();
        }
        Data data = parseJson(path);
        WriteApiBlocking writeApi = client.getWriteApiBlocking();
        //writeApi.writeMeasurement(WritePrecision.S, data.getMetadata());
        if(isValidData(data)){
            writeApi.writeMeasurement(WritePrecision.S, data.getMetrics().getNode());
            for (Process process : data.getMetrics().getProcess()) {
                writeApi.writeMeasurement(WritePrecision.S, process);
            }
        } else {
            System.out.println("Invalid data for file " + path);
        }
    }

    private boolean isValidData(Data data){
        return data != null && data.getMetrics() != null
                && data.getMetrics().getNode() != null && data.getMetrics().getProcess() != null;
    }

    public void writeDataFromDirectory(File[] listFiles) throws FileNotFoundException {
        for (File fileInDirectory : listFiles) {
            if (fileInDirectory.isFile()) {
                System.out.println(fileInDirectory.getName());
                writeData(fileInDirectory.getAbsolutePath());
            }
        }
    }

    public static Data parseJson(String path) throws FileNotFoundException {
        System.out.println("File : " + path);
        if (gson == null) {
            gson = new GsonBuilder().setLenient().create();
        }
        return gson.fromJson(new FileReader(path), Data.class);
    }

    public static void initClient() {
        client = InfluxDBClientFactory.createV1(System.getenv("INFLUXDB_URL"),
                System.getenv("INFLUXDB_USER"),
                System.getenv("INFLUXDB_USER_PASSWORD").toCharArray(),
                System.getenv("INFLUXDB_DB"),
                "autogen");
    }

    public void closeClient() {
        if (client != null) {
            client.close();
        }
    }
}
