#!/bin/bash
# This script generate random stress test
CPU=$(shuf -i 0-3 -n 1)
IO=$(shuf -i 0-3 -n 1)
VM=$(shuf -i 0-2 -n 1)
sudo stress --cpu ${CPU} --io ${IO} --vm ${VM} --vm-bytes 256M --timeout 35s