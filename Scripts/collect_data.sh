#!/usr/bin/env bash

#------------------- SCRIPT VARS (Do not edit)-------------------------
CURRENT_DATETIME=$(date +"%d-%m-%YT%T%:z")
CURRENT_TIMESTAMP=$(date +"%s")
#--------------------------------------------------------

#------------------- USER VARS --------------------------
FOLDER_PATH=~/METRICS
FILE_NAME=metric_${CURRENT_TIMESTAMP}.json
#---------------------------------------------------------

#------------------ FUNCTIONS --------------------
function getDatas() {
    jq -n --arg time ${CURRENT_DATETIME} --arg host $(hostname) --arg load_cpu $(cat /proc/loadavg | awk '{print $1}') -f main_template.jq > /tmp/${FILE_NAME}
    METRICS=$(ps -o pid,rss,cputimes,command ax | grep -v PID | awk '/[0-9]*/{print $1 ";" $2 ";" $3 ";" $4}')

    for i in ${METRICS}
    do
        PID=$(echo $i | cut -d ';' -f1)
        RSS=$(echo $i | cut -d ';' -f2)
        CPUTIMES=$(echo $i | cut -d ';' -f3)
        COMMAND=$(echo $i | cut -d ';' -f4)

        cat <<< $(jq '.metrics.process += [{"host": "'$(hostname)'","pid":'${PID}',"name":"'${COMMAND}'","rss":'${RSS}',"cputime":'${CPUTIMES}'}]' /tmp/${FILE_NAME}) > /tmp/${FILE_NAME}

    done
    mv /tmp/${FILE_NAME} ${FOLDER_PATH}/
}

#----------- END FUNCTIONS ------------------------

#----------- MAIN ---------------------------------
[ -d ${FOLDER_PATH} ] || mkdir -p ${FOLDER_PATH}
getDatas