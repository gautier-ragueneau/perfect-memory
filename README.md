# Perfect Memory

Project for Perfect Memory. This README will explain how to deploy all tools to collect data from a server A and send it to server B that it will be store and displays collected metrics from server A. 

You can also read my [workbook](https://gitlab.com/gautier-ragueneau/perfect-memory/-/blob/main/perfect_memory.md) to understand how I built and designed this solution. 

- [Perfect Memory](#perfect-memory)
- [Grafana/InfluxDB server](#grafanainfluxdb-server)
  - [Install prerequisites](#install-prerequisites)
  - [Deploy Grafana / InfluxDB](#deploy-grafana--influxdb)
- [Master server](#master-server)
  - [Data collecting (bash script)](#data-collecting-bash-script)
  - [Data sending (Java Deamon)](#data-sending-java-deamon)
- [Grafana configuration](#grafana-configuration)
  - [Add InfluxDB datasource](#add-influxdb-datasource)
  - [Import dashboard](#import-dashboard)
- [TODO](#todo)

First, we need to create two VMs (you can also use your own machine) and install prerequisites softwares.
Go to your favorite VM cloud provider (or local) and create two centos 8 VM. One to collect and send metrics, and an other one to store and display metrics

I used AWS with EC2 to quickly create VMs.

# Grafana/InfluxDB server
We will deploy our monitoring stack. 

## Install prerequisites
Connect on your **metric server** (VM) and run following commands to install docker and deploy influxDB and Grafana
```bash
$ sudo yum install -y yum-utils
# Install git
$ sudo yum install -y git
# Install docker
$ sudo yum-config-manager     --add-repo     https://download.docker.com/ linux/centos/docker-ce.repo
$ sudo yum install -y docker-ce docker-ce-cli containerd.io
$ sudo groupadd docker
$ sudo usermod -aG docker $USER
$ sudo systemctl enable docker.service
$ sudo systemctl enable containerd.service
# Install docker-compose
$ sudo curl -L "https://github.com/docker/compose/releases/download/2.2.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
# Clone our git repo
$ git clone https://gitlab.com/gautier-ragueneau/perfect-memory.git perfect_memory
# Create folders to deploy our monitoring stack
$ mkdir -p perfect_memory/Docker/{grafana,influxdb}
```

## Deploy Grafana / InfluxDB

First, you need de create `.env` file used by `docker-compose.yml` in `perfect_memory/Docker/` folder:
```bash
$ cat .env
INFLUXDB_DB=perfect_memory
INFLUXDB_USER=pm_user
INFLUXDB_USER_PASSWORD=pm2021
GF_SECURITY_ADMIN_USER=grafana_user
GF_SECURITY_ADMIN_PASSWORD=grafana_password
```

```bash
$ tree -L 2
|-- perfect_memory
    |-- Docker
    |   |-- .env
    |   |-- docker-compose.yml
    |   |-- grafana
    |   `-- influxdb
```
```
$ pwd
/home/ec2-user/perfect_memory/Docker
$ docker-compose up -d
$ docker ps
CONTAINER ID   IMAGE               COMMAND                  CREATED      STATUS             PORTS                                       NAMES
ffa1ecbddba3   grafana/grafana     "/run.sh"                2 days ago   Up About an hour   0.0.0.0:3000->3000/tcp, :::3000->3000/tcp   grafana
94f786145ca3   influxdb:1.8        "/entrypoint.sh infl…"   2 days ago   Up About an hour   0.0.0.0:8086->8086/tcp, :::8086->8086/tcp   influxdb
```

At this stage, influxDB and Grafana run well.
With AWS EC2 you can acces to your service using its public DNS or publique IP.

`http://<ip_public>:3000` for Grafana

`http://<ip_public>:8086` for InfluxDB
> It's better to use https with a signed certificate. See [Perfect memory #Improvement](https://gitlab.com/gautier-ragueneau/perfect-memory/-/blob/main/perfect_memory.md#improvements)

# Master server
On this server, you will run scripts to collect the data and send it to InfluxDB.

Connect to your **master server** and run same commands as your **metric server**:
[Install prerequisites](#install-prerequisites)

On your **master server** you need to install `jq` to play with JSON in your bash script
```
$ sudo yum install epel-release -y
$ sudo yum update -y
$ sudo yum install jq -y
```

## Data collecting (bash script)
You can choose output folder of your metrics :

Edit `perfect_memory/Scripts/collect_data.sh` and modify `FOLDER_PATH` according to the output path you want
```
FOLDER_PATH=/home/ec2_user/METRICS
FILE_NAME=metric_${CURRENT_TIMESTAMP}.json
```
> You can also edit `FILE_NAME` var if you want a different output files for metrics. 

Make the script executable :
```
$ chmod +x ~/perfect_memory/Scripts/collect_data.sh
```

Let's test if everything works, run the script :
```
$ cd /perfect_memory/Scripts/ && ./collect_data.sh
```

Add `collect_data.sh` to the crontab to run it every minutes. Use `contrab -e` to edit the cron jobs and add the following line according to the path of your script
```
* * * * * cd /perfect_memory/Scripts/ && ./collect_data.sh
```
Every minute, the script will run and write a new file (In the format: `metric_<timestamp>.json`) in `FOLDER_PATH` defined previously. 

## Data sending (Java Deamon)

Go to `perfect_memory/Java/perfect_memory_client/` and add our `.env` file with our secrets :
```
INFLUXDB_DB=perfect_memory
INFLUXDB_USER=influxdb_user
INFLUXDB_USER_PASSWORD=influxdb_password
INFLUXDB_URL=https://your_url:8086
```

Next step is to run following commands to build and deploy our java deamon in docker : 

```
$ docker build -t writer_perfect_memory .
$ docker-compose up -d
Creating writer_perfect_memory ... done
$ docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS          PORTS     NAMES
f5e52bb0056f   writer_perfect_memory   "java -jar writer.ja…"   21 seconds ago   Up 14 seconds             writer_perfect_memory
```

You can see that files are processing using ```docker logs writer_perfect_memory```


# Grafana configuration

In this state, **master** sends host metrics to the **metric** server. Metrics from **master** are stored in InfluxDB. 

Now we can display metrics in Grafana. 

Connect to Grafana using your user / password configured in [Deploy Grafana / InfluxDB](#deploy-grafana--influxdb)

## Add InfluxDB datasource
Go to left pannel -> Configuration -> Data sources -> Add data source -> Select InfluxDB

All informations are in your `.env` file in `~/perfect_memory/Java/perfect_memory_client/.env` 
- URL : `INFLUXDB_URL` (or http://influxdb:8086 locally)
- Database : `$INFLUXDB_DB`
- User : `$INFLUXDB_USER`
- Password : `$INFLUXDB_USER_PASSWORD`

Save and test

## Import dashboard

You can import predefined `Perfect Memory Dashboard`: 

    Left pannel -> + (Create) -> Import -> Upload JSON file and choose the JSON file 'Grafana/Perfect Memory Dashboardjson.json' in the git repository. 

You can have a beer :beer: or whatever you want, you're done. You can now view the host metrics from a remote Grafana dashboard. 

# TODO
- More and better Unit tests



